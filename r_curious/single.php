<?php get_header(); ?>

<div id="contents_wrap">
<?php get_template_part('part-title'); ?>
<?php get_template_part('part-pan'); ?>
<div class="wrapper">
	<div id="contents">
		<section class="news_entry">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article <?php post_class(); ?>>
				<div class="entry-header">
					<p class="pt_l">
						<time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
							<?php the_time( 'Y.m.d'  ); ?>
						</time>
					</p>
					<h3 class="entry-title">
						<?php the_title(); ?>
					</h3>
				</div>
				<section class="entry-content">
					<?php the_content(); ?>
				</section>
				<ul class="page_link cf">
					<li class="prev">
						<?php previous_post_link('%link', '« 前の記事へ', false); ?>
					</li>
					<li class="next">
						<?php next_post_link('%link', '次の記事へ »', false); ?>
					</li>
				</ul>
			</article>
			<?php endwhile; endif; ?>
			<?php wp_reset_query(); ?>
			<p class="linkbtn3 mb_l"><a href="<?php bloginfo('url'); ?>/newslist/">一覧を見る</a></p>
		</section>
	</div>
	<!-- contents -->

</div>
<!-- wrapper -->
</div>
<?php get_footer(); ?>
