<?php get_header(); ?>

<?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
?>

<div id="contents_wrap">
  <?php get_template_part('part-title'); ?>
  <div id="contents">
  <?php
    $parent_slug = '';
    if($post->post_parent){
      $parent_slug = get_page_uri($post->post_parent).'-';
    }

    $page = TEMPLATEPATH.'/part-'.$parent_slug.$post->post_name.'.php';

    if (file_exists($page)) {
      include ($page);
    }?>
    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; ?>
    <?php else : ?>
    <?php include (TEMPLATEPATH . '/404.php'); ?>
    <?php endif; ?>

      
    <?php if(!is_front_page()):?>
    <?php include (TEMPLATEPATH.'/bottom_contact.php'); ?>
    <?php endif; ?>
      

  </div>
  <!--contents_wrap --> 
</div>
<!--contents -->
<?php get_footer(); ?>