<section id="bottom_contact" class="bg_green pt pb enter-bottom">
    <div class="wrapper">
        <div class="outer cf pt pb">
            <div class="left">
                <p>
                    レンタカーのご予約については<br class="pc">
                    こちらからご相談下さい</p>
            </div>
            <div class="right">
                <div class="outer cf"><img src="<?php bloginfo('template_url'); ?>/images/tel_mark.svg"><span class="num">011-768-5100</span></div>
                <div class="time">営業時間：10:00~20:00　火曜定休</div>
            </div>
        </div>

    </div>
    <!-- wrapper -->
</section>
