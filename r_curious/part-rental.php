<section class="rental pt_l">

    <section class="top_bnr pt_l pb_l">
        <div class="wrapper">
            <ul class="outer cf">
                <li class="left">
                    <a href="<?php bloginfo('url'); ?>/guide/"><img src="<?php bloginfo('template_url'); ?>/images/price_top_bnr1.jpg" alt="ご利用・返却の際はお伺い致します"></a>
                </li>
                <li class="right">
                    <a href="<?php bloginfo('url'); ?>/rental/#01"><img src="<?php bloginfo('template_url'); ?>/images/price_top_bnr2.jpg" alt="当日プラン￥2,980～ レンタル可能"></a>
                </li>
            </ul>
            <p class="mt_s pt_s"><a class="bnr3" href="<?php bloginfo('url'); ?>/lineup/"><img src="<?php bloginfo('template_url'); ?>/images/price_top_bnr3.jpg" alt="重機レンタルリース 特設ページ"></a></p>
        </div>
        <!-- wrapper -->
    </section>
    <!-- security -->

    <section class="cartype" id="01">
        <div class="wrapper">
            <h3 class="headline1 pt_l">レンタカーの車種と料金</h3>

            <ul>
                <li class="col1 mb">
                    <h4>軽自動車・軽トラック<span class="cc">（660cc）</span></h4>

                    <div class="sp car-photo"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype1.png"></div>
                    <table cellspacing="0" cellpadding="0">
                        <col span="4" />
                        <tr>
                            <td class="pc" rowspan="2"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype1.png"></td>
                            <th class="h24">24時間</th>
                            <th class="day">当日</th>
                            <th class="add">追加1日</th>
                        </tr>
                        <tr>
                            <td class="h24"><span class="en">￥</span>4,900</td>
                            <td class="day"><span class="en">￥</span>2,980</td>
                            <td class="add"><span class="en">￥</span>3,900</td>
                        </tr>
                    </table>
                </li>
                <li class="col2 mb">
                    <h4>コンパクトカー<span class="cc">（1500cc）</span></h4>
                    <div class="sp car-photo"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype2.png"></div>
                    <table cellspacing="0" cellpadding="0">
                        <col span="4" />
                        <tr>
                            <td class="pc" rowspan="2"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype2.png"></td>
                            <th class="h24">24時間</th>
                            <th class="day">当日</th>
                            <th class="add">追加1日</th>
                        </tr>
                        <tr>
                            <td class="h24"><span class="en">￥</span>6,900</td>
                            <td class="day"><span class="en">￥</span>4,980</td>
                            <td class="add"><span class="en">￥</span>5,900</td>
                        </tr>
                    </table>
                </li>

                <li class="col3 mb">
                    <h4>ステーションワゴン</h4>
                    <div class="sp car-photo"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype3.png"></div>
                    <table cellspacing="0" cellpadding="0">
                        <col span="4" />
                        <tr>
                            <td class="pc" rowspan="2"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype3.png"></td>
                            <th class="h24">24時間</th>
                            <th class="day">当日</th>
                            <th class="add">追加1日</th>
                        </tr>
                        <tr>
                            <td class="h24"><span class="en">￥</span>7,900</td>
                            <td class="day"><span class="en">￥</span>5,980</td>
                            <td class="add"><span class="en">￥</span>6,900</td>
                        </tr>
                    </table>
                </li>

                <li class="col4 mb">
                    <h4>ミニバン</h4>
                    <div class="sp car-photo"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype4.png"></div>
                    <table cellspacing="0" cellpadding="0">
                        <col span="4" />
                        <tr>
                            <td class="pc" rowspan="2"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype4.png"></td>
                            <th class="h24">24時間</th>
                            <th class="day">当日</th>
                            <th class="add">追加1日</th>
                        </tr>
                        <tr>
                            <td class="h24"><span class="en">￥</span>12,900</td>
                            <td class="day"><span class="en">￥</span>9,880</td>
                            <td class="add"><span class="en">￥</span>10,800</td>
                        </tr>
                    </table>
                </li>

                <li class="col5">
                    <h4>おしごと車<span class="cc">（キャラバン・ハイエース）</span></h4>
                    <div class="sp car-photo"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype5.png"></div>
                    <table cellspacing="0" cellpadding="0">
                        <col span="4" />
                        <tr>
                            <td class="pc" rowspan="2"><img src="<?php bloginfo('template_url'); ?>/images/price_cartype5.png"></td>
                            <th class="h24">24時間</th>
                            <th class="day">当日</th>
                            <th class="add">追加1日</th>
                        </tr>
                        <tr>
                            <td class="h24"><span class="en">￥</span>16,800</td>
                            <td class="day"><span class="en">￥</span>15,800</td>
                            <td class="add"><span class="en">￥</span>12,800</td>
                        </tr>
                    </table>
                </li>
            </ul>

            <div class="menseki mt">
                <h5 class="mb_s">免責保証　￥1,080 ~ </h5>
                <p><span class="about">当日とは</span>…10時貸し出し。返却は20時迄。</p>
                <p><span class="about">24時間プランとは</span>…前日19時以降から借りて、早朝出発。返却は営業時間内。</p>
                <!--                <p class="kome">※ 貸し出し追加は、最大2日迄</p>-->
            </div>

            <p class="linkbtn2 col2 pb_l"><a href="<?php bloginfo('url'); ?>/guide/">ご利用ガイド</a></p>

        </div>
    </section>

    <section class="calendar" id="01">
        <div class="wrapper">
            <h3 class="headline1 pt_l pb">空車状況カレンダー</h3>
            <ul class="grid_col2 sp1 cf">
                <li class="col mb">
                    <h4 class="headline5">軽自動車・軽トラック</h4>
                    <div class="outer-frame">
                        <iframe src="https://calendar.google.com/calendar/embed?src=fa1k6k35c0itqp1knbsmrkm7ag%40group.calendar.google.com&ctz=Asia%2FTokyo&showTitle=0&showPrint=0&showTabs=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
                    </div>
                </li>
                <li class="col mb">
                    <h4 class="headline5">コンパクトカー（1500ccまで）</h4>
                    <div class="outer-frame">
                        <iframe src="https://calendar.google.com/calendar/embed?src=pm0gb3cf0apgj3ph1b7ku70jf4%40group.calendar.google.com&ctz=Asia%2FTokyo&showTitle=0&showPrint=0&showTabs=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
                    </div>
                </li>
                <li class="col mb">
                    <h4 class="headline5">ステーションワゴン</h4>
                    <div class="outer-frame">
                        <iframe src="https://calendar.google.com/calendar/embed?src=4d9mfoqof6rchm3vf8mi792m68%40group.calendar.google.com&ctz=Asia%2FTokyo&showTitle=0&showPrint=0&showTabs=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
                    </div>
                </li>
                <li class="col mb">
                    <h4 class="headline5">ミニバン</h4>
                    <div class="outer-frame">
                        <iframe src="https://calendar.google.com/calendar/embed?src=v1e0209ng4eutf0dgisa9mkrhk%40group.calendar.google.com&ctz=Asia%2FTokyo&showTitle=0&showPrint=0&showTabs=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
                    </div>
                </li>
                <li class="col">
                    <h4 class="headline5">おしごと車</h4>
                    <div class="outer-frame">
                        <iframe src="https://calendar.google.com/calendar/embed?src=4hmjobc80ndntqr0d7lrrmqc84%40group.calendar.google.com&ctz=Asia%2FTokyo&showTitle=0&showPrint=0&showTabs=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
                    </div>
                </li>
            </ul>
        </div>
    </section>


</section>
<!--- mantenance -->
