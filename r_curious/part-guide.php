<section id="top-title" class="<?php echo $post->post_name; ?> bg_green pt ">
    <h3 class="headline enter-bottom">レンタカー</h3>
    <p class="pt pb">軽自動車からワゴン車まで、用途に応じてご用意いたします。ご利用・返却の際はお伺いいたしますので、ラクラクです。<br class="pc">（一部の地域は対象外）
        <!--会員入会の方は最大20％off！-->(ETC装着車、チャイルドシートもございます)</p>
    <h4>JR稲積公園駅にお届け可能！</h4>
    <div class="obi mt_l enter-left"></div>
</section>


<section class="guide pt_l bg_img1 pb_s">
    <div class="wrapper">
        <h3 class="headline1">レンタカーご利用ガイド</h3>
        <dl class="step1 red mt enter-bottom">
            <dt>ご来店</dt>
            <dd>
                <div class="outer cf">
                    <div class="left">
                        <h4 class="headline4">お持ちいただくもの</h4>
                        <ol>
                            <li>1. <span class="red">運転免許証</span>（運転される方全員）</li>
                            <li>2. お支払金額<br><span class="small">（現金またはクレジットカードにて、出発時に予定金額を申し受けます）</span></li>
                        </ol>
                        <img class="baloon1" src="<?php bloginfo('template_url'); ?>/images/rentacar_step1_baloon.svg" alt="※現地でのカード清算も可能です。">
                        <img class="card" src="<?php bloginfo('template_url'); ?>/images/rentacar_step1_card.png" alt="AMERICAN EXPRESS | VISA | Mastercard | JCB">
                    </div>
                    <!-- left -->
                    <div class="right">
                        <img src="<?php bloginfo('template_url'); ?>/images/rentacar_step1_photo1.svg" alt="運転免許証 & お支払金額">
                    </div>
                    <!-- right -->
                </div>
                <!-- outer -->
            </dd>
        </dl>

        <dl class="step2 green mt enter-bottom">
            <dt>ご返却</dt>
            <dd>
                <h4 class="headline4">ガソリンは満タンにしてご返却ください</h4>
                <p class="pb">ガソリンは満タンにて貸し出しいたします。<br class="pc">返却時に最寄のガソリンスタンドで給油をお願いいたします。（給油されていない場合は、走行距離に応じて所定の燃料代を申し受けます。）</p>
                <h4 class="headline4">スタッフの確認と精算</h4>
                <p class="pb">スタッフが傷の有無をチェックし、超過時間などの料金精算を行います。</p>
                <h4 class="headline4">超過料金と違約金について</h4>
                <p>万が一予定時間を過ぎる場合は、必ず事前にお電話にてご連絡ください。<br class="pc">超過料金を別途申し受けます。なお、連絡無く予定時間を超過した場合には、所定の違約料金をお支払いいただきますのでご注意ください。</p>
            </dd>
        </dl>

        <dl class="step3 red mt enter-bottom">
            <dt>保険内容・補償制度について</dt>
            <dd>
                <h4 class="headline4">万一の事故の際も、限度額の範囲で保証金が給付されます。</h4>
                <div class="sp"><img class="baloon2 img_center" src="<?php bloginfo('template_url'); ?>/images/rentacar_step3_photo2_sp.svg"></div>
                <div class="photo mb_s">
                    <img class="pc baloon2" src="<?php bloginfo('template_url'); ?>/images/rentacar_step3_photo2.svg">
                    <img class="pc img_center" src="<?php bloginfo('template_url'); ?>/images/rentacar_step3_photo1.svg" alt="対人賠償１名無制限（免責10万円） | 対物賠償１事故無制限（免責10万円） | 人身損害保険3,000万円（自賠責保険も含む）">
                    <img class="sp img_center" src="<?php bloginfo('template_url'); ?>/images/rentacar_step3_photo1_sp.svg" alt="対人賠償１名無制限（免責10万円） | 対物賠償１事故無制限（免責10万円） | 人身損害保険3,000万円（自賠責保険も含む）">
                </div>
                <p class="pt_s">免責事項に該当する事故は、<span class="red">お客様の負担</span>となります。</p>
            </dd>
        </dl>

        <dl class="step4 green mt enter-bottom">
            <dt>免責補償制度</dt>
            <dd>
                <img class="tree" src="<?php bloginfo('template_url'); ?>/images/rentacar_step4_tree.svg">
                <h4>ご出発前にこの制度に加入する事ができます。</h4>
                <table cellspacing="0" cellpadding="0">
                    <tr class="standard">
                        <th>標準コース ￥<span class="en">1,080</span></th>
                        <td>
                            <div class="cf">
                                <p class="left">対物免責額・車輌免責額&nbsp;&nbsp;<span class="underline red bold">支払い免除</span></p>
                                <div class="right"><img class="baloon2" src="<?php bloginfo('template_url'); ?>/images/rentacar_step4_photo1.svg" alt="故障の場合はロードサービス実費"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="safe">
                        <th>安心コース ￥<span class="en">2,160</span></th>
                        <td>
                            <p>対物免責額・車輌免責額&nbsp;&nbsp;<span class="underline red bold">&nbsp;&nbsp;支払い免除&nbsp;&nbsp;</span>&nbsp;&nbsp;+&nbsp;&nbsp;<span class="underline bold">ロードサービス</span> </p>
                            <div class="pc"><img class="pt_s img_center baloon2" src="<?php bloginfo('template_url'); ?>/images/rentacar_step4_photo2.svg" alt="バッテリーの上がり | タイヤのパンク（スペアタイヤ交換） | ガス欠（※ガソリン代実費） | キーの閉じ込み"></div>
                            <div class="sp"><img class="pt_s img_center baloon2" src="<?php bloginfo('template_url'); ?>/images/rentacar_step4_photo2_sp.svg" alt="バッテリーの上がり | タイヤのパンク（スペアタイヤ交換） | ガス欠（※ガソリン代実費） | キーの閉じ込み"></div>
                        </td>
                    </tr>
                </table>
            </dd>
        </dl>

        <dl class="step5 red mt enter-bottom">
            <dt>保険金が支払われない場合</dt>
            <dd>
                <div class="outer cf">
                    <div class="photo">
                        <img src="<?php bloginfo('template_url'); ?>/images/rentacar_step5_photo.png">
                    </div>
                    <!-- photo -->
                    <div class="text">
                        <ul>
                            <li>・警察の事故証明の無い場合及び保険約款の免責事項に該当する場合。</li>
                            <li>・無断延長時での事故または貸渡契約時以外の方の運転での事故。</li>
                            <li>・タイヤパンク交換修理費用。</li>
                            <li>・無謀運転（故意によるもの）</li>
                            <li>・当て逃げなど</li>
                            <li>・ホイールキャップ＆キーの紛失など</li>
                        </ul>
                    </div>
                    <!-- text -->
                </div>
                <!-- outer -->
            </dd>
        </dl>

        <dl class="step6 green mt enter-bottom">
            <dt>ノン・オペレーション・チャージについて</dt>
            <dd>
                <p class="pb_s">万一事故を起こされ、車輌修理が必要になった場合、修理期間中の営業補償の一部として<br class="pc">下記金額をご負担いただきます。</p>
                <div class="outer cf pt_s">
                    <div class="left pb_s matchheight">
                        <img class="pt_s pb_s img_center" src="<?php bloginfo('template_url'); ?>/images/rentacar_step6_photo1.svg">
                        <p class="text-center">レンタカーで</p>
                        <h4><span class="red bold">自走して予定の店舗に返還された場合</span></h4>
                        <h5 class="en">￥20,000</h5>
                    </div>
                    <!-- left -->
                    <div class="right pb_s matchheight">
                        <img class="pt_s pb_s img_center" src="<?php bloginfo('template_url'); ?>/images/rentacar_step6_photo2.svg">
                        <p class="text-center">レンタカーで</p>
                        <h4><span class="red bold">自走できずに予定の店舗に返還できなかった場合</span></h4>
                        <h5 class="en"><span class="kome1">※保安基準に該当する損傷</span>￥50,000</h5>
                    </div>
                    <!-- right -->
                </div>
                <p class="kome2">※ノンオペレーションチャージは、免責補償制度加入でもご負担いただきます。<br class="pc">レッカー移動費用作業費用は別途お客様負担になります。</p>
            </dd>
        </dl>

        <p class="linkbtn2 col2"><a href="<?php bloginfo('url'); ?>/">レンタカーTOP</a></p>


    </div>
    <!-- wrapper -->
</section>
<!--- mantenance -->
