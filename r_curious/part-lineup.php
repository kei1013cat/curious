<section class="lineup">
    <section class="title pb_l">
        <div class="wrapper">
            <h3 class="headline1 pt enter-bottom">重機機械シーズンレンタル・リース</h3>
            <p class="title-text">弊社では皆様の多彩なニーズに応える為に、小型、中型、大型などの重機を取り揃えております。特に雪国では除雪も含めて活躍の機会が多い重機をレンタルという形で、皆様に気軽に使って頂ければ幸いです。料金などは下記より参考にして頂き、その他ご不明点やご質問などが御座いましたら、弊社までご連絡下さい。</p>
        </div>
    </section>
    <!-- title -->

    <section class="rental">
        <section class="top">
            <div class="grid_ptn1 type1 cf enter-left">
                <div class="photo col"></div>
                <div class="text col">
                    <div class="outer">
                        <h3>小型クラス</h3>
                        <p>CAT 901　HITACHI　ZW30他</p>
                        <h4 class="price">月<span class="en">￥98,000 ~ </span>（税+保険）</h4>
                        <p class="pt_s">CAT 902　HITACHI　ZW40</p>
                        <h4 class="price">月<span class="en">￥108,000 ~ </span>（税+保険）</h4>
                    </div>
                </div>
                <!-- text -->
            </div>
            <!-- grid-ptn1 -->

            <div class="grid_ptn1 type2 cf enter-right">
                <div class="photo col"></div>
                <div class="text col">
                    <div class="outer">
                        <h3>中型クラス</h3>
                        <p>CAT 907　HITACHI　ZW80他</p>
                        <h4 class="price">月<span class="en">￥198,000 ~ </span>（税+保険）</h4>
                    </div>
                </div>
                <!-- text -->
            </div>
            <!-- grid-ptn1 -->
        </section>
        <!-- top -->

        <section class="bottom pt_l pb_l enter-bottom">
            <div class="wrapper">
                <div class="outer cf">
                    <div class="left">
                        <img src="<?php bloginfo('template_url'); ?>/images/rental_oogata_photo.jpg">
                    </div>
                    <div class="right">
                        <h3>大型クラス</h3>
                        <h4 class="price pb_s">価格：￥248,000 ~ <span class="small">/月</span></h4>
                        <p>KOMATSU　WA100<br>HITACHI　　ZW100</p>
                    </div>
                </div>
            </div>
        </section>

        <div class="wrapper">
            <p class="linkbtn2 col2"><a href="<?php bloginfo('url'); ?>/guide/">ご利用ガイド</a></p>
        </div>


    </section>
    <!--- rental -->
</section>
