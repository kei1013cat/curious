<?php
$url = $_SERVER['REQUEST_URI'];
$parent_id = $post->post_parent; // 親ページのIDを取得
$parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得

?>
<?php
$parent_slug = '';
if($post->post_parent){
  $parent_slug = get_page_uri($post->post_parent).'_';
}
$imagesrc = 'pagetitle_'.$parent_slug.$post->post_name.'.jpg';

?>


<?php if(is_single() ||  $post->post_name =="information"):?>

    <div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_news.jpg) no-repeat center center; background-size:cover;">
        <div class="bg-mask">
        </div>
    </div>

<?php else: ?>


    <div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/<?php echo $imagesrc; /*$image_tmp = get_field('タイトル画像');if( !empty($image_tmp) ){ echo $image_tmp['sizes']['pagetitle'];}*/?>) no-repeat center center; background-size:cover;">
        <div class="bg-mask">
        </div>
    </div>
<?php endif; ?>
