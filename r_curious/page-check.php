<?php include "config.php"; ?>
<?php get_header(); ?>

<div id="contents_wrap">
  <div id="contents">
    <section>
      <div class="wrapper">
        <h3 class="headline1">見出し1<span class="small">headline1</span></h3>
<h3 class="headline2">見出し2<span class="small">headline2</span></h3>

<p>&nbsp;</p>
<hr>
<p>&nbsp;</p>
 <p class="linkbtn1"><a href="#">linkbtn1</a></p>
<p>&nbsp;</p>
<hr>
<p>&nbsp;</p>
        <ul class="grid_col2 sp1 cf">
          <li style="background: #ccc;">col2 sp1</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
        </ul>
        <p>&nbsp;</p>
        <ul class="grid_col3 sp2 cf">
          <li style="background: #ccc;">col3 sp2</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
        </ul>
        <p>&nbsp;</p>
        <ul class="grid_col4 tab3 sp2 cf">
          <li style="background: #ccc;">col4 tab3 sp2</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
          <li style="background: #ccc;">&nbsp;</li>
        </ul>
      </div>
      <!--wrapper --> 
    </section>
  </div>
  <!--contents_wrap --> 
</div>
<!--contents -->
<?php get_footer(); ?>
