(function ($) {
	$.dropdown = function (options) {
		var o = $.extend({
			onTarget: 'header nav li',
			drop: '.drop',
			onImg: '',
			speed: 250 //slow,normal,fast
		}, options);
		$(o.onTarget).hover(
		function () {
			$(o.drop, this).stop(true, true).slideDown(o.speed);
		}, function () {
			$(o.drop, this).slideUp(o.speed);
		});
	},
	$.rollover = function (options) {
		var o = $.extend({
			ovTarget: 'img.over, input.over',
			ovImg: '_ov',
			onImg: '_on'
		}, options);
		$(o.ovTarget).not('[src*="' + o.onImg + '."]').each(function () {
			var src = $(this).attr('src');
			var ovSrc = src.replace(/\.[^.]+$/, o.ovImg + '$&');
			$(this).hover(function () {
				$(this).attr('src', ovSrc);
			}, function () {
				$(this).attr('src', src);
			});
			$(window).unload(function () {
				$(this).attr('src', src);
			});
		});
	};
	$(function () {
		$.dropdown();
		$.rollover();
	});
})(jQuery);