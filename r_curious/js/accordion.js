  $(function(){
  $("#acMenu dt").on("click", function() {
    $(this).next().slideToggle(); 
    // activeが存在する場合
    if ($(this).children(".accordion_icon").hasClass('active')) {
      // activeを削除
      $(this).children(".accordion_icon").removeClass('active');
    }
    else {

      // アクティブ中のアコーディオンーを探索し、存在すれば強制的に閉じる
    	$("#acMenu dt").each(function(i, elem) {
    		if ($(this).children(".accordion_icon").hasClass('active')) {
          	// アコーディオンを閉じる
            $(this).click();
        	}
    	});

      $(".accordion_icon").removeClass('active');
      // activeを追加
      $(this).children(".accordion_icon").addClass('active');
    }
  });
});