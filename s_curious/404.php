    <?php get_header(); ?>

    <div id="main_contents" class="cf">
        <div id="contents" class="mr15">
            <div class="wrapper">
                <section class="tac">
                    <div class="text-outer">
                        お探しのページは見つかりませんでした<br>
                        <a href="<?php bloginfo('url'); ?>">トップページより再度アクセスしてください</a>
                    </div>
                </section>
            </div>
            <!-- wrapper -->
        </div>
        <!-- contents -->

    </div>
    <!-- main_contents -->

    <?php get_footer(); ?>
