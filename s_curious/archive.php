<?php get_header(); ?>

<div id="contents">
    <?php include (TEMPLATEPATH . '/part-title.php'); ?>
    <section class="news_list">
        <div class="wrapper cf">

            <div class="wrapper tac">
                <h2 class="headline02"><span>お知らせ一覧</span></h2>
            </div>
            <div class="left_contents">
                <?php if ( have_posts() ) :?>
                <section class="news">
                    <div class="lay_postlist2">
                        <?php while ( have_posts() ) : the_post(); ?>

                        <dl class="cf">
                            <dt><?php the_time('Y年m月d日'); ?><br></dt>
                            <dd><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                    <h3><?php if(mb_strlen($post->post_title)>27) { $title= mb_substr($post->post_title,0,27) ; echo $title. '...' ;
} else {echo $post->post_title;}?><?php
    $category = get_the_category();
    $cat_name = $category[0]->cat_name;
    $cat_slug = $category[0]->category_nicename;

?>
                                        <?php if($cat_name!='未分類'):?><span class="cat"><?php echo $cat_name; ?></span><?php endif; ?></h3>
                                    <p>
                                        <?php echo get_the_custom_excerpt( get_the_content(), 90 ) ?></p>
                                </a></dd>
                        </dl>

                        <?php endwhile; ?>
                    </div>
                </section>
                <div class="pagination">
                    <?php echo bmPageNaviGallery(); // ページネーション出力 ?>
                </div>
                <!-- pagination -->
                <?php else : ?>
                記事が見つかりません。
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
            <!-- left_cont -->

        </div>
        <!-- wrapper -->
    </section>
</div>
<!-- contents -->

<?php get_footer(); ?>
