<section id="top-title" class="<?php echo $post->post_name; ?> bg_green pt ">
    <h3 class="headline enter-bottom">自動車修理・車検</h3>
    <p class="pt">自動車の修理をはじめとした、当社のサービスをご案内いたします。</p>
    <div class="obi mt enter-left"></div>
</section>


<section class="mantenance pt_l pb bg_img1">

    <div class="wrapper">
        <dl class="red mt">
            <dt>板金・塗装（自動車板金）</dt>
            <dd class="outer cf photo-right">
                <div class="photo fead">
                    <div class="inner">
                        <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg" /></div>
                        <img src="<?php bloginfo('template_url'); ?>/images/mantenance_photo1.jpg" />
                    </div>
                </div>
                <!-- photo -->
                <div class="text">
                    <p><span class="red">熟練の技術とサービス</span>で、市内屈指の顧客満足度を誇ります。
                        パール、メタリック、焼付、マジョーラなど塗装全般の作業から、気になるキズ・ヘコミまで、クルマのプロが、入念に作業にあたります。
                        お車をお預かりの期間中は、当社のレンタカーをご利用ください。</p>
                    <p class="linkbtn1 mt"><a href="http://www.curious-r.com/">レンタカーについて詳しく見る</a></p>
                </div>
                <!-- text -->
            </dd>
        </dl>
        <dl class="green">
            <dt>車検</dt>
            <dd class="outer cf photo-left">
                <div class="photo fead">
                    <div class="inner">
                        <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg" /></div>
                        <img src="<?php bloginfo('template_url'); ?>/images/mantenance_photo2.jpg" />
                    </div>
                </div>
                <!-- photo -->
                <div class="text">
                    <p>これからも長く乗りたい愛車だからこそ、しっかりとした車検を受けたいものです。 そんなあなたには、<span class="red">カーショップクリオスの安心車検</span>がオススメ！
                        <span class="red">24か月法定点検付の安心車検</span>に、嬉しい代車無料サービスつき！スピーディーで丁寧なサービスも、ご好評をいただいております。
                    </p>
                </div>
                <!-- text -->
            </dd>
        </dl>
        <dl class="red">
            <dt>整備</dt>
            <dd class="outer cf photo-right">
                <div class="photo fead">
                    <div class="inner">
                        <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg" /></div>
                        <img src="<?php bloginfo('template_url'); ?>/images/mantenance_photo3.jpg" />
                    </div>
                </div>
                <!-- photo -->
                <div class="text">
                    <h3 class="pb_s">車のパーツや設備などの整備はお任せ下さい！</h3>
                    <p>リサイクルパーツなど中古車パーツとの使い分けでコストを抑えたり、車検や修理とあわせて対応することも可能です。
                        お気軽にご相談ください。
                    </p>
                </div>
                <!-- text -->
            </dd>
        </dl>
    </div>
    <!-- wrapper -->

</section>
<!--- mantenance -->
