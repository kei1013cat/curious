<section id="sidebar">
    <ul class="cf">
        <li class="bnr1 mb_s pt"><a href="https://www.its-tea.or.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr1.jpg" alt="ITS-TEA | 一般社団法人ITSサービス高度化機構" /></a></li>
        <li class="bnr2 mb_s pt_s"><a href="https://www.aioinissaydowa.co.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr2.jpg" alt="あいおいニッセイ同和損保代理店" /></a></li>
        <li class="stock bnr3 mb_s pt_s cf">
            <div class="left">
                <a href="https://www.goo-net.com/usedcar_shop/0303577/stock.html" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr3_left.jpg" alt="Gooネット在庫情報">
                </a>
            </div>
            <div class="right">
                <a href="https://www.carsensor.net/shop/hokkaido/211370001/stocklist/" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr3_right.jpg" alt="カーセンサー在庫情報">
                </a>
            </div>
        </li>
        <li class="bnr4 pt_s pb"><a href="https://cf.cedyna.co.jp/sim/addon/pc/index.asp?pay=120&in0=1" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr4.jpg" alt="ローンシミュレーションはこちら"></a></li>

    </ul>

</section>


<section id="sidebar-sp">
    <ul class="cf">
        <li class="bnr1"><a href="https://www.its-tea.or.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr1.jpg" alt="ITS-TEA | 一般社団法人ITSサービス高度化機構" /></a></li>
        <li class="bnr2"><a href="https://cf.cedyna.co.jp/sim/addon/pc/index.asp?pay=120&in0=1" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr4.jpg" alt="ローンシミュレーションはこちら"></a></li>
    </ul>
    <ul class="cf">
        <li class="bnr3"><a href="https://www.aioinissaydowa.co.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr2.jpg" alt="あいおいニッセイ同和損保代理店" /></a></li>
        <li class="stock bnr4">
            <div class="cf">
                <div class="left">
                    <a href="https://www.goo-net.com/usedcar_shop/0303577/stock.html" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr3_left.jpg" alt="Gooネット在庫情報">
                    </a>
                </div>
                <!-- left -->
                <div class="right">
                    <a href="https://www.carsensor.net/shop/hokkaido/211370001/stocklist/" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr3_right.jpg" alt="カーセンサー在庫情報">
                    </a>
                </div>
                <!-- right -->
            </div>
            <!-- cf -->
        </li>

    </ul>

</section>
