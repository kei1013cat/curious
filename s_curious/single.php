<?php get_header(); ?>

<div id="contents_wrap">
<?php get_template_part('part-title'); ?>
<?php get_template_part('part-pan'); ?>
	<div id="contents">
        <section id="top-title" class="<?php echo $post->post_name; ?> bg_green pt ">
            <h3 class="headline enter-bottom">お知らせ</h3>
            <div class="obi mt enter-left"></div>
        </section>
        <section class="bg_img1">
            <div class="wrapper">
            <section class="news_entry enter-bottom">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <article <?php post_class(); ?>>
                    <div class="entry-header">
                        <h3 class="entry-title">
                            <time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
                                <?php the_time( 'Y.m.d'  ); ?>
                            </time>
                            <?php the_title(); ?>
                        </h3>
                    </div>
                    <section class="entry-content">
                        <?php the_content(); ?>
                    </section>
                    <ul class="page_link cf">
                        <li class="prev">
                            <?php previous_post_link('%link', '« 前の記事へ', false); ?>
                        </li>
                        <li class="next">
                            <?php next_post_link('%link', '次の記事へ »', false); ?>
                        </li>
                    </ul>
                </article>
                <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
            </section>
            </div>
        </section>
	</div>
	<!-- contents -->

</div>
<?php get_footer(); ?>
