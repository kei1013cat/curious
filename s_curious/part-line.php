<section id="top-title" class="<?php echo $post->post_name; ?> bg_green pt ">
    <h3 class="headline enter-bottom">LINEお問い合わせ</h3>
    <div class="obi mt enter-left"></div>
</section>


<section class="line">

    <section class="line_top pt_l pb_l">
        <div class="wrapper">
            <div class="outer cf">
                <div class="left">
                    <img class="enter-left" src="<?php bloginfo('template_url'); ?>/images/line_official.svg" alt="">
                </div>
                <!-- left -->
                <div class="right">
                    <h3><img src="<?php bloginfo('template_url'); ?>/images/line_official_headline.svg" alt="クリオス公式 LINE@ | LINEトークで簡単お問い合わせ！"></h3>
                    <ul class="pt">
                        <li>遠方のお客様も安心！</li>
                        <li>動画や写真も送受信可能で分かりやすい！</li>
                        <li>見積や車検のお問い合わせもトークで簡単！</li>
                    </ul>
                </div>
                <!-- right -->
            </div>
            <!-- outer -->
        </div>
        <!-- wrapper -->
    </section>
    <!-- line_top -->

    <section class="line_account pb_l">
        <div class="wrapper">
            <h3 class="mt mb">クリオス公式アカウントなら</h3>
            <ul class="cf enter-bottom">
                <li class="matchheight">
                    <dl class="no1">
                        <dt><span class="text">1</span></dt>
                        <dd>
                            <img class="arrow" src="<?php bloginfo('template_url'); ?>/images/line_under_arrow.svg" />
                            <div class="outer">
                                <img src="<?php bloginfo('template_url'); ?>/images/line_point_icon1.svg" />
                                <p>遠方のお客様にオススメです！</p>
                            </div>
                        </dd>
                    </dl>
                </li>
                <li class="matchheight">
                    <dl class="no2">
                        <dt><span class="text">2</span></dt>
                        <dd>
                            <img class="arrow" src="<?php bloginfo('template_url'); ?>/images/line_under_arrow.svg" />
                            <div class="outer">
                                <img src="<?php bloginfo('template_url'); ?>/images/line_point_icon2.svg" />
                                <p>LINEで制限無く画像や動画のやり取りが可能です！</p>
                            </div>
                        </dd>
                    </dl>
                </li>
                <li class="matchheight">
                    <dl class="no3">
                        <dt><span class="text">3</span></dt>
                        <dd>
                            <img class="arrow" src="<?php bloginfo('template_url'); ?>/images/line_under_arrow.svg" />
                            <div class="outer">
                                <img src="<?php bloginfo('template_url'); ?>/images/line_point_icon3.svg" />
                                <p>納車後の相談ツールとしてもオススメです！</p>
                            </div>
                        </dd>
                    </dl>
                </li>
            </ul>
        </div>
        <!-- wrapper -->
    </section>
    <!-- account -->

    <section class="talk bg_gray pt pb">
        <div class="wrapper">
            <img class="pc enter-bottom" src="<?php bloginfo('template_url'); ?>/images/line_talk.png" />
            <img class="sp enter-bottom" src="<?php bloginfo('template_url'); ?>/images/line_talk_sp.png" />
        </div>
        <!-- wrapper -->
    </section>

    <section class="line_add pt_l pb">
        <div class="wrapper">
            <h3 class="enter-bottom">友だち追加方法</h3>
            <ul class="cf enter-bottom">
                <li class="matchheight">
                    <dl class="no1">
                        <dt>スマートフォンの方は<br>こちらをクリック！</dt>
                        <dd>
                            <a href="line://ti/p/@bqs1194a"><img class="pt_s" alt="友だち追加" src="<?php bloginfo('template_url'); ?>/images/line_add.svg"></a>
                        </dd>
                    </dl>
                </li>
                <li class="matchheight">
                    <dl class="no2">
                        <dt>LINEの友だち追加から<br>【ID検索】で登録</dt>
                        <dd class="account">bps1194a</dd>
                    </dl>
                </li>
                <li class="matchheight">
                    <dl class="no3">
                        <dt>LINEの友だち追加から<br>【QRコード】で登録</dt>
                        <dd>
                            <img src="<?php bloginfo('template_url'); ?>/images/line_qr.svg">
                        </dd>
                    </dl>
                </li>
            </ul>
        </div>
    </section>
    <!-- line_add -->

</section>
<!--- LINE -->
