<section id="top-title" class="<?php echo $post->post_name; ?> bg_green pt ">
    <h3 class="headline enter-bottom">アフターフォロー</h3>
    <div class="obi mt enter-left"></div>
</section>


<section class="afterfollow pt_l">

    <section class="security pt_l pb_l">
        <div class="wrapper">
            <h3 class="headline2 enter-top">保証内容<span class="line"></span></h3>
            <div class="outer cf">
                <div class="photo enter-right">
                    <img src="<?php bloginfo('template_url'); ?>/images/afterfollow_security_photo.jpg">
                </div>
                <!-- photo -->
                <div class="text enter-bottom">
                    <p>
                        <span class="red">保証期間は保証書記載の点検整備を完了した日から１ヶ月、または点検整備したときからの走行距離が1000kmに達した時のいずれか早い時点までとなります。</span><br class="pc">
                        保証期間中、主要部品に不具合が現れ、当社がその欠陥を認めた時は、<span class="red">当該部品の部品代及び取替工賃を全額負担いたします。</span>
                    </p>
                </div>
                <!-- text -->
            </div>
            <!-- outer -->

            <div class="bottom enter-left">
                <h4 class="pb_s">【保証適用外事項】</h4>
                <ol class="pb_l">
                    <li>１．当社または当社指定工場以外で修理された場合</li>
                    <li>２．車高の変更など車輌の機能に悪影響を与える改造が施された場合</li>
                    <li>３．取扱上、使用上の酷使、間違い、または事故によって生じた故障</li>
                    <li>４．塗装面の退色、メッキ類のサビ、スプリング類の故障など</li>
                    <li>５．機能上、影響の無い音・振動・オイルのにじみなど</li>
                    <li>６．自動車が使用できないことによる損失（休業補償、商業損失、レンタカー代など）</li>
                </ol>

                <h5><span class="under">最長3年の保証も用意しております。（別途料金）</span></h5>
                <p class="pt_s">ご要望の方はスタッフまでお申し出ください。 </p>
                <p class="linkbtn1 mt"><a href="https://www.carsensor.net/trust/" target="_blank">詳しくはこちらをクリック</a></p>
            </div>
        </div>
        <!-- wrapper -->
    </section>
    <!-- security -->

    <section class="accident bg_img1 pt_l pb_l">
        <div class="wrapper">
            <h3 class="headline2 enter-top">もしも事故にあったら<span class="line"></span></h3>
            <h4>突発的なアクシデントに混乱するとは思いますが、まずは落ち着いて行動することが大切です。<br class="pc">事故状況の確認、医師の診断は何よりも優先してください。</h4>

            <div class="flow">
                <dl class="enter-bottom">
                    <dt><span class="num">1</span>安全の確保</dt>
                    <dd>ケガ人がいる場合は安全な路肩などに移動し、<span class="red">救急車を手配</span>します。<br class="pc">
                        また二次災害を防止するため、発煙筒、停止表示機材を設置するなど、<span class="red">安全保持</span>に努めましょう。</dd>
                </dl>
                <img class="underarrow img_center pt_s pb_s enter-top" src="<?php bloginfo('template_url'); ?>/images/under_arrow.svg">
                <dl class="enter-bottom">
                    <dt><span class="num">2</span>警察へ届ける</dt>
                    <dd>加害者からの報告は義務ですが、<span class="red">被害者が届け出ることも必要</span>です。(とくにケガを負った場合は｢人身扱い｣の届出が重要です。) <br class="pc">
                        また、届け出の警察署より、交通事故証明書の交付を受けましょう。</dd>
                </dl>
                <img class="underarrow img_center pt_s pb_s enter-top" src="<?php bloginfo('template_url'); ?>/images/under_arrow.svg">
                <dl class="enter-bottom">
                    <dt><span class="num">3</span>保険会社へ事故の報告</dt>
                    <dd>事故の当事者双方が、加入している<span class="red">保険会社に連絡</span>しましょう。任意保険に入っていない場合は自賠責保険の会社へ連絡します。<br class="pc">
                        緊急連絡先は車検証と一緒に車内（助手席側のグローブボックスなど）に保管されているはずです。</dd>
                </dl>
                <img class="underarrow img_center pt_s pb_s enter-top" src="<?php bloginfo('template_url'); ?>/images/under_arrow.svg">
                <dl class="enter-bottom">
                    <dt><span class="num">4</span>カーショップクリオスへご連絡ください！</dt>
                    <dd>カーショップクリオスへご連絡ください！自走できない事故車は、搬送車で引き取りに伺いいたします。<br class="pc">
                        また、 事故車の修理はもちろん、代車の手配もおまかせください！<br class="pc">
                        事故や故障などの急なトラブルにも、<span class="red">無料で保険完備のレンタカーの貸し出しが可能</span>です。<br class="pc">
                        交通事故のご相談も承っております。ご相談ください。</dd>
                </dl>
            </div>
            <!-- flow -->

            <dl class="service mt_l enter-bottom">
                <dt>レッカーサービス</dt>
                <dd>
                    <div class="outer cf">
                        <div class="text">
                            走行が出来なくなったお車などのレッカー移動も行っております。<br class="pc">
                            万が一の際には安心ダイヤル加盟店のカーショップクリオスが対応致しますので、お気軽にお電話ください。
                        </div>
                        <!-- text -->
                        <div class="photo">
                            <img src="<?php bloginfo('template_url'); ?>/images/accident_service.svg">
                        </div>
                        <!-- photo -->
                    </div>
                    <!-- outer -->
                </dd>
            </dl>
        </div>
        <!-- wrapper -->
    </section>
    <!-- accident -->

</section>
<!--- mantenance -->
