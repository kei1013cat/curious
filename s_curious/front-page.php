<?php get_header(); ?>

<?php include (TEMPLATEPATH . '/sidelink.php'); ?>

<div id="contents_wrap">
    <div id="contents">
        <section class="mainvisual" id="mainvisual">
            <div class="vegas-outer">
                <div style="display:none;" id="vegas-text">
                    <p id="vegas-msg">
                        <div id="text"></div>
                    </p>
                </div>
            </div>
        </section>
        <!-- mainvisual -->

        <section class="top_menu pb bg_green enter-bottom">
            <div class="wrapper">
                <h2 class="pt pb enter-top"><img src="<?php bloginfo('template_url'); ?>/images/top_menu_title.svg" alt="中古車販売のカーショップクリオス | クルマのことなら全ておまかせください！"></h2>

                <ul class="grid_col3 tab3 sp1 cf">
                    <li class="col">
                        <h4 class="baloon enter-bottom1">安心のサポート</h4>
                        <a href="http://www.curious-r.com/">
                            <figure class="hover-snip cf">
                                <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_menu1.jpg"></div>
                                <figcaption>
                                    <h2>レンタカー</h2>
                                    <div class="caption">
                                        <p>READ MORE</p>
                                    </div>
                                </figcaption>
                            </figure>
                        </a>
                        <p class="text">軽自動車からワゴン車まで、用途に応じてご用意いたします。ご利用・返却の際はお伺いいたしますので、ラクラクです。</p>
                    </li>
                    <li class="col">
                        <h4 class="baloon enter-bottom1">信頼の技術</h4>
                        <a href="<?php bloginfo('url'); ?>/mantenance/">
                            <figure class="hover-snip cf">
                                <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_menu2.jpg"></div>
                                <figcaption>
                                    <h2>自動車修理・車検</h2>
                                    <div class="caption">
                                        <p>READ MORE</p>
                                    </div>
                                </figcaption>
                            </figure>
                        </a>
                        <p class="text">高い顧客満足度を誇る熟練の技術とサービスで、お客様のカーライフをしっかりとサポートいたします。事故や故障などの急なトラブルにも、保険完備のレンタカーの貸し出しが可能です。</p>
                    </li>
                    <li class="col">
                        <h4 class="baloon enter-bottom1">もしもの時も安心</h4>
                        <a href="<?php bloginfo('url'); ?>/afterfollow/">
                            <figure class="hover-snip cf">
                                <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_menu3.jpg"></div>
                                <figcaption>
                                    <h2>アフターフォロー</h2>
                                    <div class="caption">
                                        <p>READ MORE</p>
                                    </div>
                                </figcaption>
                            </figure>
                        </a>
                        <p class="text">当社は自動車保険にも力を入れており、事故･故障などの急なトラブルには、レンタカーなどで迅速に対応いたします。</p>
                    </li>
                </ul>
            </div>
        </section>
        <!-- top_menu -->

        <section class="sales pt_l pb">
            <div class="wrapper">
                <h3 class="headline1 enter-top">中古車販売・新車販売</h3>
                <dl class="enter-bottom1">
                    <dt class="red">中古車販売</dt>
                    <dd>ご予算、ご希望の車種などをお伺いして、全国よりお客様のご希望にあった車をお探し致します<br class="pc">全国産メーカー全車種取扱い可能です。</dd>
                </dl>
                <dl class="enter-bottom1">
                    <dt class="green">新車販売</dt>
                    <dd>全国産メーカー全車種取扱い可能です。詳しくは当店へお問合わせください。</dd>
                </dl>
                <dl class="enter-bottom1">
                    <dt class="red">取扱いローン会社</dt>
                    <dd>オリエントコーポレーション／セディナ／ＳＢＩクレジット／プレミアファイナンス<br class="pc">（各種カードも取扱い可能です。）</dd>
                </dl>
            </div>
            <!-- wrapper -->
        </section>
        <!-- sales -->

        <section class="kaitori bg_gray pt_l">
            <div class="wrapper">
                <h3 class="headline2 enter-top">中古車買取<span class="line"></span></h3>

                <div class="outer cf">
                    <div class="photo enter-right">
                        <img src="<?php bloginfo('template_url'); ?>/images/index_kaitori_photo.jpg">
                    </div>
                    <!-- photo -->
                    <div class="text enter-bottom">
                        <p>お車を高く買取致します！事故有・事故現状・ローン継続中・手続きは安心の５日以内！<br class="pc">
                            書類手続き・名義変更無料！お電話下さい！<br class="pc">
                            また、無料での出張査定による中古車買取や、事故車の買取もいたしております。お気軽にお問合わせください。<br class="pc">
                            <span class="red">ホイルローダー、除雪機なども強化買取中！</span></p>
                    </div>
                    <!-- text -->
                </div>

            </div>
            <!-- wrapper -->

        </section>
        <!-- kaitori -->

        <section class="news">

            <div id="contents_outer" class="cf">
                <div id="main_contents">

                    <?php
            $wp_query = new WP_Query();
            $param = array(
            'posts_per_page' => '8', //表示件数。-1なら全件表示
            'post_status' => 'publish',
            'orderby' => 'date', //ID順に並び替え
            'order' => 'DESC'
            );
            $wp_query->query($param);?>
                    <?php if($wp_query->have_posts()):?>
                    <section class="news pb_l">
                        <h3 class="mt_l news-header enter-bottom1 mb_s">お知らせ<span class="small">NEWS</span></h3>
                        <?php while($wp_query->have_posts()) :?>
                        <?php $wp_query->the_post(); ?>
                        <dl class="cf news_list enter-bottom1">
                            <dt><?php the_time('Y.m.d'); ?></dt>
                            <dd>
                                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php echo $post->post_title; ?></a>
                            </dd>
                        </dl>
                        <?php endwhile; ?>
                        <!-- inner -->
                    </section>
                    <!-- news -->
                    <?php endif; ?>

                </div>
                <!-- main_contents -->
                <div>
                    <!-- left_contents -->
                    <?php get_sidebar(); ?>
                </div>
                <!-- pc -->
            </div>
            <!-- contents_outer -->

        </section>



    </div>
    <!--contents -->
</div>
<!--contents_wrap -->
<?php get_footer(); ?>
