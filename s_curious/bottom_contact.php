<section id="bottom_contact" class="bg_green pt pb">
  <div class="wrapper">
      <div class="outer cf pt pb enter-bottom">
          <div class="left">
              <p>中古車販売・新車販売・車検・修理<br class="pc">
              お車のことならなんでもご相談下さい</p>
          </div>
          <div class="right">
              <div class="outer cf"><img src="<?php bloginfo('template_url'); ?>/images/tel_mark.svg" ><span class="num">011-768-5557</span></div>
              <div class="time">営業時間：10:00~20:00　火曜定休</div>
          </div>
      </div>
      
  </div>
  <!-- wrapper -->
</section>