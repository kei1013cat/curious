<footer>

    <div class="footer-tel sp bg_green">
        <div class="wrapper">
            <h3 class="pb"><span class="underline">営業時間：10:00~20:00　火曜定休</span></h3>
            <p><a href="tel:0117685557"><span class="text">今すぐ電話をする</span><span class="num">011-768-5557</span></a></p>
            <h4 class="pt_s pb_l">中古車販売・新車販売・車検・修理、お車のことならなんでもご相談下さい</h4>
        </div>
    </div>

    <div class="footer-top cf">
        <div class="wrapper">
            <div class="footer_logo">
                <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.svg" alt="中古車販売のカーショップクリオス | 札幌市北区新川"></a>
            </div>
            <!-- footer_top -->
            <div class="footer_contact">
                <address>札幌市北区新川西2条4丁目8番18号</address>
                <div class="inner cf">
                    <img class="tel" src="<?php bloginfo('template_url'); ?>/images/tel_mark.svg"><span class="num"><a href="tel:0117685557">011-768-5557</a></span>
                    <div class="time">営業時間：10:00~20:00　火曜定休</div>
                </div>
            </div>
            <!-- footer_contact -->
        </div>
        <!-- wrapper -->
    </div>

    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d707.268106279459!2d141.2773101659995!3d43.12614728953663!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0b2646fa640e4f%3A0xa80743cb458479e1!2z44Kr44O844K344On44OD44OXIOOCr-ODquOCquOCuQ!5e0!3m2!1sja!2sjp!4v1551716720871" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <nav>
        <ul class="cf">
            <li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
            <li><a href="<?php bloginfo('url'); ?>/about/">お店について</a></li>
            <li><a target="_blank" href="https://www.goo-net.com/usedcar_shop/0303577/stock.html">在庫情報</a></li>
            <li><a href="http://www.curious-r.com/">レンタカー</a></li>
            <li><a href="<?php bloginfo('url'); ?>/mantenance/">自動車修理・車検</a></li>
            <li><a href="<?php bloginfo('url'); ?>/afterfollow/">アフターフォロー</a></li>
            <li><a href="<?php bloginfo('url'); ?>/line/">LINEお問い合わせ</a></li>
        </ul>
    </nav>

    <p class="copy">Copyright &copy;CAR SHOP CURIOUS All Rights Reserved.</p>

</footer>
<p id="page_top"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/pagetop.svg" alt="pagetop"></a></p>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
<?php wp_footer(); ?>
</main>
</div>
<!--outer -->
<script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>
</body>

</html>
