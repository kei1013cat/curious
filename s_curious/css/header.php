<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo '  '; }
 bloginfo('name'); ?>
</title>
<meta name="keyword" content=""/>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
<?php if(is_pc()):?>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
    <![endif]-->
<?php endif; ?>
<?php //以下user設定 ?>
<script src="<?php bloginfo('template_url'); ?>/js/jquery-3.3.1.min.js"></script>

<!-- scrollreveal -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>

<!-- accordion -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/accordion.js"></script>
<!-- smoothScroll -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
<!-- pulldown -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>

<!-- matchHeight -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/matchHeight_userdf.js"></script>

<!-- rwdImageMaps -->
<script src="<?php bloginfo('template_url'); ?>/js/rwdImageMaps/jquery.rwdImageMaps.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/rwdImageMaps_userdf.js"></script>

<!-- drawer -->
<script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/drawer/dist/css/drawer.min.css">
<script src="<?php bloginfo('template_url'); ?>/js/drawer/dist/js/drawer.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/drawer_userdf.js"></script>

<script type="text/javascript">
jQuery( function( $ ) {
  $( '#mw_wp_form_mw-wp-form-90 select option[value=""]' )
  .html( '選択してください' );
} );
jQuery( function( $ ) {
  $( '#mw_wp_form_mw-wp-form-61 select option[value=""]' )
  .html( '選択してください' );
} );
</script>

<script>
$(function() {
    //製品名の自動選択
    <?php if ($_GET["product"] != NULL && $post->post_type =="contact"): ?>
        $('#product').val('<?php echo $_GET['product']; ?>');
    <?php endif; ?>
});
</script>

<!-- etc -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/product_apply.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/ajaxzip3_userdf.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/privacyarea_proc.js"></script>

<?php if(is_front_page()): ?>

<!-- bxslider -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/bxslider/jquery.bxslider.min.css">

<!-- slick -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick-theme.css" type="text/css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/slick_userdf.js"></script>

<?php endif; ?>


<?php wp_head(); ?>
</head>
<?php
  $body_id = "";
  $body_class = "";
  if ( is_front_page() ) {
    $body_id = ' id="page_index"';
  }else if ( is_page() ) {
    if($post -> post_parent == 0 ){
        $body_id = ' id="page_'.$post->post_name.'"';
    }else{
        $ancestors =  $post-> ancestors;
        foreach($ancestors as $ancestor){
            $body_id = ' id="page_'.get_post($ancestor)->post_name.'"';
            break;
        }
    }
    $body_class = ' subpage';
    if( $post->post_parent){
        $body_class = ' '.$post->post_name;
    }
  }else if (  is_single() ) {
    $body_id = ' id="page_single"';
   $body_class = " subpage ".get_post_type( $post );
  }else if ( is_archive() ) {
    $body_id = ' id="page_archive"';
    $body_class = " subpage ".get_post_type( $post );
  }else if ( is_404() ) {
    $body_id = ' id="page_404"';
    $body_class = ' subpage';
  }
?>
<body<?php echo $body_id; ?> class="drawer drawer--top<?php echo $body_class; ?>">
<div id="outer">
<header>
  <div class="pcmenu cf">
    <div class="header_top cf">
    	<div class="wrapper">
			<h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="" /></a></h1>
			<nav>
			    <ul class="cf">
			      <li><a href="<?php bloginfo('url'); ?>/company/"><span>会社情報</span></a></li>
			      <li><a href="<?php bloginfo('url'); ?>/shop/"><span>全国販売</span></a></li>
			      <li><a href="<?php bloginfo('url'); ?>/stock/"><span>在庫</span></a></li>
			      <li><a href="<?php bloginfo('url'); ?>/concept/"><span>コンセプト</span></a></li>
			      <li><a href="<?php bloginfo('url'); ?>/staff/"><span>スタッフ</span></a></li>
			      <li><a href="<?php bloginfo('url'); ?>/reviews/"><span>口コミ</span></a></li>
			    </ul>
			  </nav>
		</div>
		<!-- wrapper -->
    </div>
    <!-- header_top -->
    <div class="header_bottom cf">
    	<div class="box">
	    	<div class="tel">
	    		<img src="<?php bloginfo('template_url'); ?>/images/header_telmark.png" alt="" />011-776-3222
	    	</div>
	    	<!-- tel -->
	    	<div class="time">
	    		受付時間：10:00～20:00
	    	</div>
	    	<!-- time -->
	    </div>
    </div><!-- header_bottom -->
  </div>
  <!-- pcmenu -->
  <div class="spmenu drawermenu" role="banner" id="top">
    <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="genba21.com | CONSTRUCTION SOLUTION" /></a></h1>
    <button type="button" class="drawer-toggle drawer-hamburger">
      <span class="sr-only">toggle navigation</span>
      <span class="drawer-hamburger-icon"></span>
    </button>
    <nav class="drawer-nav" role="navigation">
      <div class="inner">
      <ul class="drawer-menu">
        <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/">ホーム</a></li>
        <li>
          <dl id="acMenu">
            <dt>製品一覧<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
              <ul class="drawer-menu">
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/products/"> -&nbsp;&nbsp;製品TOP</a></li>
                <li><a class="drawer-menu-item" href="http://www.calsmaster.com/" target="_blank"> -&nbsp;&nbsp;現場編集長CALSMASTER</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/products/photomasterplus/"> -&nbsp;&nbsp;PHOTOMASTER Plus</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/products/sk1plus/"> -&nbsp;&nbsp;SK-1 Plus</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/products/gcdoboku/"> -&nbsp;&nbsp;現場DEカメラ土木版</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/products/genbadecamera/"> -&nbsp;&nbsp;現場DEカメラPRO（建築版）</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>//products/photomaster/"> -&nbsp;&nbsp;現場編集長PHOTOMASTER（廉価版）</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>//products/togosystem/"> -&nbsp;&nbsp;群馬県公共工事電子納品統合システム</a></li>
              </ul>
            </dd>
          </dl>
        </li>
        <li>
          <dl id="acMenu">
            <dt>サービス一覧<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
              <ul class="drawer-menu">
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/service/"> -&nbsp;&nbsp;サービスTOP</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/service/cimservice/"> -&nbsp;&nbsp;CIMデータ作成サービス</a></li>
                <li><a class="drawer-menu-item" href="https://minnanoiji.net/" target="_blank"> -&nbsp;&nbsp;道路維持管理システム Easy維～持～！</a></li>
                <li><a class="drawer-menu-item" href="https://www.haikinmaster.com/" target="_blank"> -&nbsp;&nbsp;配筋検査システム 配筋マスター</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/service/haikin/"> -&nbsp;&nbsp;配筋検査リスト作成サービス</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/service/development/"> -&nbsp;&nbsp;受託開発のご案内</a></li>
              </ul>
            </dd>
          </dl>
        </li>
        <li>
          <dl id="acMenu">
            <dt>サポート<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
              <ul class="drawer-menu">
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/supports/"> -&nbsp;&nbsp;サポートTOP</a></li>
                <li><a class="drawer-menu-item" href="http://www.calsmaster.com/support" target="_blank"> -&nbsp;&nbsp;現場編集長 CALSMASTER</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/supports/photomasterplus/"> -&nbsp;&nbsp;PHOTOMASTER Plus</a></li>
                <li><a class="drawer-menu-item" href="https://www.genba21.com/skregssl/" target="_blank"> -&nbsp;&nbsp;SK-1 Plus</a></li>
                <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/supports/genbadecamera/"> -&nbsp;&nbsp;現場DEカメラ関連</a></li>
              </ul>
            </dd>
          </dl>
        </li>
        <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/download/">ダウンロード</a></li>
        <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/link/">リンク</a></li>
        <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/inquiry/">見積依頼</a></li>
        <li class="color"><a class="drawer-menu-item" href="https://www.calsmaster.com/hoshu/genba21_agree.html" target="_blank">資料請求</a></li>
        <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/apply/">お申し込み</a></li>
        <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
      </ul>
    </div>
    <!-- inner -->
    </nav>
  </div>
  <!-- spmenu --> 
  
</header>
<main role="main">
