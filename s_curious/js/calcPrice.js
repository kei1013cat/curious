const c_tax_rate = 8; // 消費税％指定

initDateSelects();

function main() {

if($('input[name=product]:checked').val()) {

  checkNullInput();

  setVal();

  chgControl();

  calcPrice();

}


}

/***********************************
  ダウンロードボタン押下処理
************************************/
$(function(){
　　$("#download").click(function(){

    disableDownloadBtn();

    $("#staff_hidden").val($('input:text[id="staff"]').val());
    $("#comp_hidden").val($('input:text[id="comp"]').val());
    $("#mail_hidden").val($('input:text[id="mail"]').val());
    $("#usercnt_hidden").val($('#usercnt').val());
    $("#ym_hidden").val($('[name=start_year] option:selected').text() + "年" + $('[name=start_month] option:selected').text()　+ "月 ～ " + $('[name=end_year] option:selected').text() + "年" + $('[name=end_month] option:selected').text()　+ "月");
    $("#estim_dt_hidden").val(getNowYMD());
    $("#tax_rate_hidden").val(c_tax_rate);
    $("#construction_hidden").val($('input:text[id="construction"]').val());
    $("#memberid_hidden").val($('input:text[id="memberid"]').val());

    if($('input:text[id="zip1"]').val()!="" || $('input:text[id="zip2"]').val()!="") {
      $("#zip_hidden").val($('input:text[id="zip1"]').val() + "-" + $('input:text[id="zip2"]').val());
    }
    $("#tel_hidden").val($('input:text[id="tel"]').val());
    $("#address_hidden").val($('input:text[id="address1"]').val());

　　});
});

/***********************************
  関数名： inputValueClear
  概要：　入力値クリア
  引数： 
  戻り値： 
************************************/
function inputValueClear() {

  initDateSelects();
  $('input:text[id="staff"]').val("");
  $('input:text[id="comp"]').val("");
  $('input:text[id="mail"]').val("");
  $('input:text[id="remail"]').val("");
  $('#usercnt').val("");
  $('#price_total_num').html(0);
  $('#privacy').prop("checked",false);
  disableDownloadBtn();

}

function setVal() {
  var product = $('input[name=product]:checked').val();
  var product_arr = product.split(',');
  $("#product_hidden").val(product_arr[0]);
  $("#productcd_hidden").val(product_arr[1]);
  $("#memberid_hidden").val($('input:text[id="memberid"]').val());

}

/***********************************
  関数名： calcPrice
  概要：　金額計算処理
  引数：
  戻り値：
************************************/
function calcPrice() {
  if($('input[name=product]:checked').val()!="") {

    $("#initial_cost_hidden").val(getPriceSeparate(objProductPrice[$("#productcd_hidden").val()]['initial_cost']));
    $("#monthly_cost_hidden").val(getPriceSeparate(objProductPrice[$("#productcd_hidden").val()]['monthly_cost']));
    $("#file_hidden").val(getPriceSeparate(objProductPrice[$("#productcd_hidden").val()]['file']));

    //税抜金額計算
    var taxExcludedAmt = calcInitialCost(objProductPrice[$("#productcd_hidden").val()]['initial_cost']) + (objProductPrice[$("#productcd_hidden").val()]['monthly_cost'] * getMonths() * userCount());

    //税込金額計算
    var totalPrice = taxExcludedAmt + taxCalc(taxExcludedAmt);

  } else {
    var totalPrice = 0;
  }

  // お見積り金額反映
  if(isNaN(totalPrice)) {
    $('#price_total_num').val(0);
    $('#price_total_hidden').val(0);
  } else {
    $('#price_total_num').html(getPriceSeparate(totalPrice));
    $('#price_total_hidden').val(getPriceSeparate(totalPrice));
  }
}

function calcInitialCost(initial_cost) {
  if (getMonths() <= 0) {
    return 0;
  } else {
    return initial_cost * (userCount() > 0 ? 1 : 0);
  }
}

/***********************************
  関数名： taxCalc
  概要：　消費税計算
  引数：
  戻り値：
************************************/
function taxCalc(price){
  var tax_price = Math.floor(price * 0.01 * c_tax_rate);
  $('#tax_hidden').val(getPriceSeparate(tax_price));
  $('#sub_total_hidden').val(getPriceSeparate(price));
  return tax_price;
}

/***********************************
  関数名： userCount
  概要：　利用人数をコントロールから取得
  引数：
  戻り値： 
************************************/
function userCount() {
  var cnt;
  if( $('#usercnt').val() < 0) {
    cnt = 0;
    $('#usercnt').val(0);
  } else {
    cnt = $('#usercnt').val();
  }
  return cnt;
//  return $('#usercnt').val() < 0 ? 0 : $('#usercnt').val();
}

/***********************************
  関数名： getMonths
  概要：　利用期間から月数を戻す
  引数： 
  戻り値： 
************************************/
function getMonths() {
  if($('#keiyaku_hidden').val()==0) {
    $('#months_hidden').val(1);
    return 1;
  }

  var start_year = $('[name=start_year] option:selected').text();
  var start_month = $('[name=start_month] option:selected').text();
  var end_year = $('[name=end_year] option:selected').text();
  var end_month = $('[name=end_month] option:selected').text();
  var months = ((end_year - start_year) * 12) + (end_month - start_month) + 1;

  $('#months_hidden').val(months < 0 ? 0 : months);
  return months < 0 ? 0 : months;

}

/***********************************
  関数名： initDateSelects
  概要：　利用期間のデフォルト選択
  引数： 　
  戻り値：
************************************/
function initDateSelects() {

  var now = new Date();
  var nYear = now.getFullYear();
  var nMonth = now.getMonth() + 1;
  var nDate = now.getDate();

  $('select[name="start_year"] option[value="'+ nYear + '"]').prop('selected',true);
  $('select[name="start_month"] option[value="'+ nMonth + '"]').prop('selected',true);
  $('select[name="end_year"] option[value="'+ nYear + '"]').prop('selected',true);
  $('select[name="end_month"] option[value="'+ nMonth + '"]').prop('selected',true);

  }
  if(window.addEventListener) {
  window.addEventListener("load"  , initDateSelects, false);
  }
  else if(window.attachEvent) {
  window.attachEvent("onload", initDateSelects);
}

/***********************************
  関数名： 
  概要：　
  引数：　
  戻り値： なし
************************************/
function chgControl() {

    //保守契約あるかチェック
    if(objProductPrice[$("#productcd_hidden").val()]['hoshu']==2) {
      $("#date_control").show();
      $('#keiyaku_hidden').val(1);
      $('#keiyaku_val_hidden').val("あり");
    } else {
      $("#date_control").hide();
      $('#keiyaku_hidden').val(0);
      $('#keiyaku_val_hidden').val("なし");
    }
}

/***********************************
  関数名： checkNullInput
  概要：　コントロールがNULLかどうかチェック
  引数： 
  戻り値： 
************************************/
function checkNullInput() {
  
  checkNumText();

  if(checkStaff() &&
     checkCompany() &&
     checkMail() &&
     checkReMail() &&
     checkUserCount() &&
     checkPrivacy() &&
     checkTextMail() &&
     checkTextRemail() &&
     checkConstruction() &&
     checkNumText() &&
     checkTel()

  ) {
/*
    pdfDownload(
      $('input:text[id="staff"]').val()
      ,$('input:text[id="comp"]').val()
      ,$('input:text[id="mail"]').val()
      ,$('#usercnt').val()
      ,$('[name=start_year] option:selected').text() + "年" + $('[name=start_month] option:selected').text()　+ "月 ～ " + $('[name=end_year] option:selected').text() + "年" + $('[name=end_month] option:selected').text()　+ "月"
      ,$('#price_total_hidden').val()
      ,$('#months_hidden').val()
      ,estim_dt
    );
*/
   // disableControl(true);
    if($('input[name=product]:checked').val() === undefined) {
      disableDownloadBtn();
    } else {
      //　ダウンロードボタンを表示
      enableDownloadBtn();
    }

  } else {
      //　ダウンロードボタンを非表示
      disableDownloadBtn();
  }
}

function checkTextMail() {

    mail = $('input:text[id="mail"]').val();

    var mail_regex1 = new RegExp( '(?:[-!#-\'*+/-9=?A-Z^-~]+\.?(?:\.[-!#-\'*+/-9=?A-Z^-~]+)*|"(?:[!#-\[\]-~]|\\\\[\x09 -~])*")@[-!#-\'*+/-9=?A-Z^-~]+(?:\.[-!#-\'*+/-9=?A-Z^-~]+)*' );
    var mail_regex2 = new RegExp( '^[^\@]+\@[^\@]+$' );
    if( mail.match( mail_regex1 ) && mail.match( mail_regex2 ) ) {
        // 全角チェック
        if( mail.match( /[^a-zA-Z0-9\!\"\#\$\%\&\'\(\)\=\~\|\-\^\\\@\[\;\:\]\,\.\/\\\<\>\?\_\`\{\+\*\} ]/ ) ) { return false; }
        // 末尾TLDチェック（〜.co,jpなどの末尾ミスチェック用）
        if( !mail.match( /\.[a-z]+$/ ) ) { return false; }
        return true;
    } else {
        return false;
    }
}

function isNumber(numVal){
  // チェック条件パターン
  var pattern = /^[-]?([1-9]\d*|0)(\.\d+)?$/;
  // 数値チェック
  return pattern.test(numVal);
}

function checkTextRemail() {
    mail = $('input:text[id="mail"]').val();
    remail = $('input:text[id="remail"]').val();
    if(mail==remail) {
      return true;
    } else {
      return false;
    }
}

function checkNumText() {
    usercnt = $('input:text[id="usercnt"]').val();
    return isNumber(usercnt);
}

/***********************************
  関数名： disableDownloadBtn
  概要：　ダウンロードボタンを非表示
  引数： 
  戻り値： 
************************************/
function disableDownloadBtn() {
    $("#download").addClass('disable');
    $("#download").removeClass('enable');
}

/***********************************
  関数名： enableDownloadBtn
  概要：　ダウンロードボタンを表示
  引数： 
  戻り値： 
************************************/
function enableDownloadBtn() {
    $("#download").addClass('enable');
    $("#download").removeClass('disable');
}

/***********************************
  関数名： getPriceSeparate
  概要：　金額をカンマ区切りにする
  引数： num(金額)　
  戻り値： カンマ区切りの金額
************************************/
function getPriceSeparate(num){
    return String(num).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
}

/***********************************
  関数名： isNull
  概要：  NULLの場合は第1引数をそうでない場合は第2引数を戻す
  引数：
  戻り値：
************************************/
function isNull(val1,val2,val3) {
  if(isNaN(val1)) {
    return val2;
  }
  if(val1=="" || val1==null || (isNaN(val1))){
    return val2
  } else {
    return val3;
  }
}

/***********************************
  関数名： getDiff
  概要：　２つの日付の差を求める
  引数：　date1Str,date2Str
  戻り値： daysDiff(日付の差分)
************************************/
function getDiff(date1Str, date2Str) {
  var date1 = new Date(date1Str);
  var date2 = new Date(date2Str);

  // getTimeメソッドで経過ミリ秒を取得し、２つの日付の差を求める
  var msDiff = date2.getTime() - date1.getTime();

  // 求めた差分（ミリ秒）を日付へ変換（経過ミリ秒÷(1000ミリ秒×60秒×60分×24時間)。端数切り捨て）
  var daysDiff = Math.floor(msDiff / (1000 * 60 * 60 *24));
  // 差分へ1日分加算して返却
  return ++daysDiff;
}

/***********************************
  関数名： getNowYMD
  概要：　
  引数：　
  戻り値： 
************************************/
function getNowYMD() {
  var dt = new Date();
  var y = dt.getFullYear();
  var m = ("00" + (dt.getMonth()+1)).slice(-2);
  var d = ("00" + dt.getDate()).slice(-2);
  var result = y + "年" + m + "月" + d + "日";
  return result;
}

/***********************************
 利用者人数に全角数字がされた場合には半角に変換
************************************/
$(function(){
    $(".keyword").blur(function(){
        charChange($(this));
    });

    charChange = function(e){
        var val = e.val();
        var str = val.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
        if(val.match(/[Ａ-Ｚａ-ｚ０-９]/g)){
            $(e).val(str);
            main();
        }
    }
});

function checkStaff()  { return $('input:text[id="staff"]').val() == "" ? false : true;}
function checkCompany() {  return $('input:text[id="comp"]').val() == "" ? false : true;}
function checkMail() {  return $('input:text[id="mail"]').val() == "" ? false : true;}
function checkReMail() {  return $('input:text[id="remail"]').val() == "" ? false : true;}
function checkUserCount() {  return $('#usercnt').val() == "" ? false : true;}
function checkPrivacy() {  return $("#privacy").prop("checked") ? true : false;}
function checkConstruction() {  return $('input:text[id="construction"]').val() == "" ? false : true;}
function checkZip() {  return $('input:text[id="zip"]').val() == "" ? false : true;}
function checkTel() {  return $('input:text[id="tel"]').val() == "" ? false : true;}
function checkAddress() {  return $('input:text[id="address1"]').val() == "" ? false : true;}
function checkMemberid() {  return $('input:text[id="memberid"]').val() == "" ? false : true;}

