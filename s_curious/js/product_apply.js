jQuery(function($) {
	hideControl();

	$("#product").change(function() {
	  hideControl();

		if($(this).val()=="現場編集長CALSMASTER") {
			$("#sk1plus-gp").show();
		}
	});

	$("#sk1plus_gp").change(function() {
		if($(this).val()=="新規") {
			$("#sk1plus-dp").show();
			$('#sk1plus-dp').append($('<option>').attr({ value: '選択してください' }).text('選択してください'));
			// $('#sk1plus-dp option[value="写真管理のみ"]' ).html( '写真管理のみ' );
			// $('#sk1plus-dp option[value="写真管理＋図書管理"]' ).html( '写真管理＋図書管理' );
			// $('#sk1plus-dp option[value="写真管理＋出来形管理"]' ).html( '写真管理＋出来形管理' );
			// $('#sk1plus-dp option[value="写真管理＋図書管理＋出来形管理"]' ).html( '写真管理＋図書管理＋出来形管理' );
			// $('#sk1plus-dp option[value="写真管理＋図書管理＋出来形管理＋黒板作成ツール"]' ).html( '写真管理＋図書管理＋出来形管理＋黒板作成ツール' );
		}
	});


});

function hideControl() {
	$("#sk1plus-gp").hide();
	$("#sk1plus-dp").hide();
}