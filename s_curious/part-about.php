<section id="top-title" class="<?php echo $post->post_name; ?> bg_green pt ">
    <h3 class="headline enter-bottom">お店について</h3>
    <div class="obi mt enter-left"></div>
</section>


<section class="about">
    <section class="topmessage pt_l pb_l">
        <div class="outer">
            <h3 class="headline2 enter-top">代表者ごあいさつ<span class="line"></span></h3>
            <p class="mb enter-bottom">この度、当社のホームページを見て頂き誠にありがとうございます。<br class="pc">
                当社は平成14年に設立してから、お客様、取引業者様、皆様に応援して頂き、<br class="pc">
                今年で16周年を迎えることができ、大変感謝しております。</p>
            <p class="enter-bottom">当社は自動車保険にも力を入れており、事故･故障などの急なトラブルに<br class="pc">
            レンタカーなどで迅速に対応するなど、お客様のカーライフを全力で<br class="pc">
            サポート致します。</p>
            <h4 class="pt enter-bottom1"><span class="small">取締役</span>小池 啓仁</h4>
        </div>
        <!-- outer -->
    </section>
    <!-- topmessage -->
    
    <section class="comp pt_l pb_l">
        <div class="wrapper">
            <h3 class="headline2 enter-top">会社概要<span class="line"></span></h3>
            <table class="style2 enter-bottom" cellspacing="0" cellpadding="0">
              <tr>
                <th>法人名</th>
                <td >有限会社クリオス札幌</td>
              </tr>
              <tr>
                <th>住所</th>
                <td>札幌市北区新川西２条４丁目８番１８号</td>
              </tr>
              <tr>
                <th>営業時間</th>
                <td>10:00～20:00</td>
              </tr>
              <tr>
                <th>定休日</th>
                <td>火曜日</td>
              </tr>
              <tr>
                <th>TEL</th>
                <td>011-768-5557</td>
              </tr>
              <tr>
                <th>会社設立</th>
                <td>平成14年12月</td>
              </tr>
              <tr>
                <th>事業内容</th>
                <td>自動車板金・塗装、自動車の修理・整備、車検、<br />
                中古車販売・買取、レンタカー、保険代理店</td>
              </tr>
              <tr>
                <th>加盟団体</th>
                <td>USS、JU、ホンダオークション、<br />
                社団法人自動車公正取引協議会、安心ダイヤルサービス</td>
              </tr>
            </table>
        </div>
        <!-- wrapper -->
    
    </section>
    <!-- comp -->
    
    
    <section class="access bg_gray1 pt_l pb_l">
        <div class="wrapper">
            <h3 class="headline2 enter-top mb_s">アクセス<span class="line"></span></h3>
            <p class="pb">札幌市北区新川西2条4丁目8番18号</p>
            <dl class="enter-bottom">
                <dt>交通機関情報</dt>
                <dd>地下鉄24条駅1番のりば<br class="pc">路線：北72（新川営業所、手稲高校前、前田森林公園行き）<br class="pc">新川西2条4丁目下車</dd>
            </dl>
        </div>
        <!-- wrapper -->
    </section>
    <!-- access -->
    
    <section class="shokai pt_l">
        <div class="wrapper">
            <h3 class="headline2 enter-top mb_s">お店の紹介<span class="line"></span></h3>
            <div class="grid_col2 sp1 cf pb_s enter-bottom">
                <div class="col">
                    <img src="<?php bloginfo('template_url'); ?>/images/about_shokai_photo1.jpg"  />
                    <p class="pt_s">ゆったりと過ごせる店内です</p>
                </div>
                <div class="col">
                    <img src="<?php bloginfo('template_url'); ?>/images/about_shokai_photo2.jpg"  />
                    <p class="pt_s">キッズスペース有り。お子様連れも大歓迎です</p>
                </div>
            </div>
        </div>
        <!-- wrapper -->
    </section>
    <!-- shokai -->
    
</section>
<!--- about -->
