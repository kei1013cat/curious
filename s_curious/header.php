<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo '  '; }
 bloginfo('name'); ?>
    </title>
    <meta name="keyword" content="クリオス,中古車販売,中古車買取,新車販売,車検,修理,整備,レンタカー,自動車板金・塗装,保険代理店" />
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/images/favicon/safari-pinned-tab.svg" color="#ffa243">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">


    <?php if(is_pc()):?>
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
    <![endif]-->
    <?php endif; ?>
    <?php //以下user設定 ?>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-3.3.1.min.js"></script>

    <!-- scrollreveal -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>

    <!-- accordion -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/accordion.js"></script>
    <!-- smoothScroll -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
    <!-- pulldown -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>

    <!-- matchHeight -->
    <script src="<?php bloginfo('template_url'); ?>/js/match-height/jquery.matchHeight.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/matchHeight_userdf.js"></script>

    <!-- drawer -->
    <script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/drawer/dist/css/drawer.min.css">
    <script src="<?php bloginfo('template_url'); ?>/js/drawer/dist/js/drawer.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/drawer_userdf.js"></script>

    <script type="text/javascript">
        jQuery(function($) {
            // ドロワーメニューが開いたとき
            $('.drawer').on('drawer.opened', function() {
                $('#menu-text').text('CLOSE');
            });
            // ドロワーメニューが閉じたとき
            $('.drawer').on('drawer.closed', function() {
                $('#menu-text').text('MENU');
            });
        });

    </script>

    <script>
        jQuery(function() {
            $('#loading_wrap').delay(100).fadeOut("slow");
        });

    </script>

    <!-- スライダー(vegas)-->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/vegas/vegas.css">
    <script src="<?php bloginfo('template_url'); ?>/js/vegas/vegas.js"></script>
    <script>
        $(function() {
            $('#mainvisual').vegas({
                slides: [{
                        src: 'wp/curious/wp-content/themes/curious/images/slider1<?php echo mobile_img(); ?>.jpg?v=20190215',
                        msg: '<h3>CAR SHOP CURIOUS</h3><p>中古車販売のカーショップクリオス<br>レンタカーからメンテナンスまで安心サポート</p>'
                    },
                    {
                        src: 'wp/curious/wp-content/themes/curious/images/slider2.jpg?v=20190215',
                        msg: '<h3>CAR SHOP CURIOUS</h3><p>中古車販売のカーショップクリオス<br>レンタカーからメンテナンスまで安心サポート</p>'
                    },
                    {
                        src: 'wp/curious/wp-content/themes/curious/images/slider3<?php echo mobile_img(); ?>.jpg?v=20190215',
                        msg: '<h3>CAR SHOP CURIOUS</h3><p>中古車販売のカーショップクリオス<br>レンタカーからメンテナンスまで安心サポート</p>'
                    },
                    {
                        src: 'wp/curious/wp-content/themes/curious/images/slider4<?php echo mobile_img(); ?>.jpg?v=20190215',
                        msg: '<h3>CAR SHOP CURIOUS</h3><p>中古車販売のカーショップクリオス<br>レンタカーからメンテナンスまで安心サポート</p>'
                    },
                    {
                        src: 'wp/curious/wp-content/themes/curious/images/slider5<?php echo mobile_img(); ?>.jpg?v=20190215',
                        msg: '<h3>CAR SHOP CURIOUS</h3><p>中古車販売のカーショップクリオス<br>レンタカーからメンテナンスまで安心サポート</p>'
                    }
                ],
                overlay: false,
                transition: 'flash',
                animation: 'fead2',
                transitionDuration: 1500,
                delay: 7000,
                animationDuration: 18000,
                timer: false
            });

        });

    </script>

    <?php wp_head(); ?>
</head>
<?php
  $body_id = "";
  $body_class = "";
  if ( is_front_page() ) {
    $body_id = ' id="page_index"';
  }else if ( is_page() ) {
    if($post -> post_parent == 0 ){
        $body_id = ' id="page_'.$post->post_name.'"';
    }else{
        $ancestors =  $post-> ancestors;
        foreach($ancestors as $ancestor){
            $body_id = ' id="page_'.get_post($ancestor)->post_name.'"';
            break;
        }
    }
    $body_class = ' subpage';
    if( $post->post_parent){
        $body_class = ' '.$post->post_name;
    }
  }else if (  is_single() ) {
    $body_id = ' id="page_single"';
   $body_class = " subpage ".get_post_type( $post );
  }else if ( is_archive() ) {
    $body_id = ' id="page_archive"';
    $body_class = " subpage ".get_post_type( $post );
  }else if ( is_404() ) {
    $body_id = ' id="page_404"';
    $body_class = ' subpage';
  }
?>
<body<?php echo $body_id; ?> class="drawer drawer--top<?php echo $body_class; ?>">
    <div id="loading_wrap"></div>
    <div id="outer">
        <header>
            <div class="pcmenu cf">
                <div class="header_top cf">
                    <div class="wrapper">
                        <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="中古車販売のカーショップクリオス | 札幌市北区新川" /></a></h1>
                        <div class="header_contact">
                            <address>札幌市北区新川西2条4丁目8番18号</address>
                            <div class="tel cf"><img class="mark" src="<?php bloginfo('template_url'); ?>/images/header_tel_mark.svg" />
                                <p class="num">011-768-5557</p>
                            </div>
                            <div class="time">営業時間：10:00~20:00　火曜定休</div>
                        </div>
                        <!-- header_contact -->
                    </div>
                    <!-- contact -->
                </div>
                <!-- header_top -->
                <div class="header_bottom cf">
                    <div class="wrapper">
                        <nav>
                            <ul class="cf">
                                <li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                                <li class="about"><a href="<?php bloginfo('url'); ?>/about/">お店について</a></li>
                                <li class="stock"><a href="javascript:void(0)">在庫情報</a>
                                    <ul class="drop">
                                        <li><a target="_blank" href="https://www.goo-net.com/usedcar_shop/0303577/stock.html">グーネット</a></li>
                                        <li><a target="_blank" href="https://www.carsensor.net/shop/hokkaido/211370001/stocklist/">カーセンサー</a></li>
                                    </ul>
                                </li>
                                <li class="rentacar"><a href="http://www.curious-r.com/">レンタカー</a></li>
                                <li class="mantenance"><a href="<?php bloginfo('url'); ?>/mantenance/">自動車修理・車検</a></li>
                                <li class="afterfollow"><a href="<?php bloginfo('url'); ?>/afterfollow/">アフターフォロー</a></li>
                            </ul>
                        </nav>
                    </div>
                    <!-- wrapper -->
                </div><!-- header_bottom -->
                <div class="header_bottom_dammy"></div>
            </div>
            <!-- pcmenu -->

            <div class="spmenu drawermenu" role="banner" id="top">
                <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="中古車販売のカーショップクリオス | 札幌市北区新川" /></a></h1>
                <button type="button" class="drawer-toggle drawer-hamburger">
                    <span class="sr-only">toggle navigation</span>
                    <span class="drawer-hamburger-icon"></span>
                    <div id="menu-text" class="text">MENU</div>
                </button>
                <nav class="drawer-nav" role="navigation">
                    <div class="nav-top">
                        <ul class="cf">
                            <li><a target="_blank" href="tel:0117685557"><img src="<?php bloginfo('template_url'); ?>/images/header_tel_icon.svg" alt="" /><span class="text">電話をかける</span></a></li>
                            <li><a target="_blank" href="https://goo.gl/maps/UZx4uprM4r32"><img src="<?php bloginfo('template_url'); ?>/images/header_mail_icon.svg" alt="" /><span class="text">Google Maps</span></a></li>
                        </ul>
                    </div>

                    <div class="inner">
                        <h3 class="sp-title">MENU</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/about/">お店について</a></li>
                            <li class="color"><a class="drawer-menu-item" href="http://www.curious-r.com/">レンタカー</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/mantenance/">自動車修理・車検</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/afterfollow/">アフターフォロー</a></li>
                            <li class="color"><a class="drawer-menu-item" href="line://ti/p/@bqs1194a">LINEでお問い合わせ</a></li>
                        </ul>
                        <h3 class="sp-title">在庫情報</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" target="_blank" href="https://www.goo-net.com/usedcar_shop/0303577/stock.html">グーネット</a></li>
                            <li class="color"><a class="drawer-menu-item" target="_blank" href="https://www.carsensor.net/shop/hokkaido/211370001/stocklist/">カーセンサー</a></li>
                        </ul>
                        <div class="bnr cf"><a href="<?php bloginfo('url'); ?>/line/"><img src="<?php bloginfo('template_url'); ?>/images/line_contact_sp.svg"></a>
                        </div>
                    </div>
                    <!-- inner -->
                </nav>
            </div>
            <!-- spmenu -->

        </header>
        <main role="main">
